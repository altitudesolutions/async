# alt-async

A library of useful functions supporting asynchronous processing. Mainly RxJS, but also Promises.

Requires rxjs ^6.4.0
