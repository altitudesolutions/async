import * as Message from "@altitude/error-message";
import {
    forkJoin,
    Observable,
    of,
    PartialObserver,
    ReplaySubject,
    Subscription,
    SubscriptionLike
} from "rxjs";
import { catchError, delay, map, shareReplay, take } from "rxjs/operators";

/**
 * An interface which exposes a `Promise` and its internal `resolve` and `reject` functions.
 */
export interface IPromiseExecutor<T> {
    readonly promise: Promise<T>;
    reject(reason?: any): void;
    resolve(value?: T): void;
}

interface IPermissivePromiseExecutor<T> {
    promise?: Promise<T>;
    reject?(reason?: any): void;
    resolve?(value?: T): void;
}

/**
 * A function which returns one or more `Observable` instances which may emit concurrently but which should
 * be intitiatedas a batch.
 */
export type AwaitAction = () => Observable<any> | Observable<any>[];

/**
 * Creates a new `Promise` and returns it as a `PromiseExecutor`.
 */
export function openPromise<T>(): IPromiseExecutor<T> {
    const r: IPermissivePromiseExecutor<T> = {};
    const p = new Promise<T>((resolve, reject) => {
        r.reject = reject;
        r.resolve = resolve;
    });
    r.promise = p;

    return <IPromiseExecutor<T>>r;
}

/**
 * Returns an `Observable` which emits and completes immediately.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to
 * each new suscbriber.
 */
export function immediate<T>(value?: T | void): Observable<T> {
    return of(<any>value).pipe(
        take(1),
        shareReplay({ refCount: true, bufferSize: 1 })
    );
}

/**
 * Returns an `Observable` which emits and completes following a specified timeout.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each new
 * suscbriber.
 * @param milliseconds The timeout period.
 */
export function awaitDelay<T>(
    milliseconds: number,
    value?: T | void
): Observable<T> {
    return of(<any>value).pipe(
        delay(milliseconds),
        take(1),
        shareReplay({ refCount: true, bufferSize: 1 })
    );
}

/**
 * Returns an `Observable` which executes a specifc action and then emits and completes following a specified delay
 * period.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each new
 * suscbriber.
 * @param milliseconds The timeout period.
 */
export function afterDelay<T>(
    milliseconds: number,
    action: () => T
): Observable<T> {
    if (action == undefined) {
        throw new Error(Message.argumentNull("action"));
    }

    return of(action).pipe(
        delay(milliseconds),
        map(f => f()),
        take(1),
        shareReplay({ refCount: true, bufferSize: 1 })
    );
}

/**
 * Accepts an `Observable` collection and returns an `Observable` which will emit and complete when all
 * constituent observables have emitted at least once.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each
 * new suscbriber.
 *
 * Returns `true` if all monitored observables complete without failing validation, otherwise returns `false`.
 * @param observables The observables to wait upon.
 */
export function awaitOne<T>(observables: Observable<T>[]): Observable<void> {
    const join = forkJoin<T>(...ensureAllComplete(observables)).pipe(
        take(1),
        map(() => undefined)
    );

    return observeOnce(join);
}

/**
 * Accepts an `Observable` collection and returns an `Observable<boolean>` which will emit and complete when all
 * constituent observables have emitted at least once.
 *
 * Returns an `Observable` which will emit `true` if all monitored observables complete without failing validation,
 * but will othewrwise emit `false`.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each
 * new suscbriber.
 *
 * @param observables The observables to wait upon.
 * @param validator An optional function to validate the result of each individual `Observable`.
 * If `validator` is not provided then all input observables which emit are considered to be valid irrespective of
 * their value.
 * @param mapErrorFn An optional function which is used to map any error originating from a source Observable.
 * If `mapErrorFn` is not provided, then errors will be unhandled.
 */
export function validateOne<T>(
    observables: Observable<T>[],
    validator?: (value: T) => boolean,
    mapErrorFn?: (error: any) => boolean
): Observable<boolean> {
    if (validator == undefined) validator = () => true;
    const join = forkJoin<boolean>(
        ...ensureAllCompleteWithMap(observables, validator, mapErrorFn)
    ).pipe(
        take(1),
        map((results: boolean[]) => {
            let valid = true;
            for (let i = 0; valid && i < results.length; i += 1) {
                valid = results[i];
            }

            return valid;
        })
    );

    return observeOnce(join);
}

/**
 * Accepts a sequence of one or more `AwaitAction` functions and returns an `Observable` which will emit and complete
 * only after every `AwaitAction` result has emitted at least once.
 *
 * Starting with the first `AwaitAction`, each action will be executed in the sequence it was passed in, with each
 * subsequent action invoked only after the preceding action has executed and returned an `Observable` which has
 * emitted at least once.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each
 * new subscriber.
 *
 * Note that each `AwaitAction` may itself encapsulate one or more aysnchronous processes which this function has no
 * control over the sequence of.
 * @param actions The `AwaitAction` sequence to execute.
 */
export function awaitSequence(...actions: AwaitAction[]): Observable<void> {
    if (actions == undefined) throw new Error("actions");

    return sequencer(actions);
}

/**
 * Returns an observable which will complete and emit with the next output of an input Observable.
 *
 * The resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each
 * new subscriber.
 * @param observable The input Observable.
 */
export function observeOnce<T>(observable: Observable<T>): Observable<T> {
    return observable.pipe(
        take(1),
        // Multicast the Observable so that it emits (replays) the buffered result to each new subscriber.
        shareReplay({ refCount: true, bufferSize: 1 })
    );
}

/**
 * Accepts an array of input observables and returns a corresponding array of observables each of which will complete
 * and emit with the next output of its associated input `Observable`.
 *
 * Each resulting `Observable` is multicast, meaning that the buffered result will continue to be emitted to each
 * new subscriber.
 * @param observables The input collection.
 */
export function observeEachOnce<T>(
    ...observables: Observable<T>[]
): Observable<T>[] {
    if (observables.length === 0) return [];
    const r: Observable<T>[] = [];
    observables.forEach(obs => {
        r.push(observeOnce(obs));
    });

    return r;
}

/**
 * Returns a `Subscription` which will complete and emit with the next output of an input `Observable`.
 * The resulting `Subscription` will automatically unsubscribe, and may be safely discarded without special handling.
 * @param observable The input observable.
 * @param observer An observer to handle output from the input observable.
 */
export function subscribeOnce<T>(
    observable: Observable<T>,
    observer: PartialObserver<T>
): Subscription;
/**
 * Returns a `Subscription` which will complete and emit with the next output of an input `Observable`.
 * The resulting `Subscription` will automatically unsubscribe, and may be safely discarded without special handling.
 * @param observable The input observable.
 * @param next The function to invoke when the input observable emits.
 * @param error The function to invoke to handle errors originating from the input observable.
 * @param complete The function to invoke when the input observable completes (always invoked unless an error
 * is raised).
 */
export function subscribeOnce<T>(
    observable: Observable<T>,
    next: (value: T) => void,
    error?: (error: any) => void,
    complete?: () => void
): Subscription;
/**
 * The implementation interface. See the individual overloads for usage.
 */
export function subscribeOnce<T>(
    observable: Observable<T>,
    // tslint:disable-next-line: unified-signatures
    next: PartialObserver<T> | ((value: T) => void),
    error?: (error: any) => void,
    complete?: () => void
): Subscription;
/**
 * Returns a `Subscription` which will complete and emit with the next output of an input `Observable`.
 * The resulting `Subscription` will automatically unsubscribe, and may be safely discarded without special handling.
 *
 * The implementation function. See the individual overloads for usage.
 */
export function subscribeOnce<T>(
    observable: Observable<T>,
    next: PartialObserver<T> | ((value: T) => void),
    error?: (error: any) => void,
    complete?: () => void
): Subscription {
    const observer = resolveObserver(next, error, complete);

    return observeOnce(observable).subscribe(observer);
}

// export function subscribeOnce<T>(
//     observable: Observable<T>,
//     next: PartialObserver<T> | ((value: T) => void),
//     error?: (error: any) => void,
//     complete?: () => void
// ): Subscription {
//     const observer = resolveObserver(next, error, complete);

//     let completed = false;
//     function safeComplete(): void {
//         if (completed) return;
//         completed = true;
//         if (observer.complete != undefined) observer.complete();
//     }
//     // Important! Define sub here to prevent temporal reference error occurring in subscription body!
//     // Do NOT declare const sub = observeOnce(observable).subscribe(...
//     let sub: Subscription;
//     let completedSynchronously = false;
//     sub = observeOnce(observable).subscribe(
//         (value: T) => {
//             completedSynchronously = sub == undefined;
//             if (completedSynchronously || !sub.closed) {
//                 if (observer.next != undefined) observer.next(value);
//                 safeComplete();
//             }
//             safeUnsubscribe(sub);
//         },
//         e => {
//             completedSynchronously = sub == undefined;
//             if (completedSynchronously || !sub.closed) {
//                 if (observer.error != undefined) observer.error(e);
//             }
//             safeUnsubscribe(sub);
//         },
//         () => {
//             safeComplete();
//         }
//     );

//     if (completedSynchronously) {
//         safeUnsubscribe(sub);
//     }

//     return sub;
// }

/**
 * Accepts an input `Observable` and returns an `Observable` which will complete upon the input observable's next emit.
 *
 * `Observable` completion is neccessary for the successful application of rxjs functions such as `forkJoin`,
 * and for the `Observable` `toPromise` method.
 * @param observable The input observable.
 */
export function ensureComplete<T>(observable: Observable<T>): Observable<T> {
    if (observable == undefined) {
        throw new Error(Message.argumentNull("observable"));
    }

    return observable.pipe(take(1));
}

/**
 * Accepts an `Observable` collection and returns a collection where each instance will complete upon the its next emit.
 *
 * `Observable` completion is neccessary for the successful application of rxjs functions such as `forkJoin`,
 * and for the `Observable` `toPromise` method.
 * @param observables The input collection.
 */
export function ensureAllComplete<T>(
    observables: Observable<T>[]
): Observable<T>[] {
    if (observables == undefined) {
        throw new Error(Message.argumentNull("observables"));
    }
    if (observables.length === 0) return [];
    const array = [];
    for (let i = 0; i < observables.length; i += 1) {
        const o = observables[i];
        if (o == undefined) {
            throw new Error(
                Message.argument(
                    "observables",
                    `Undefined value at index ${i}.`
                )
            );
        }
        array.push(o.pipe(take(1)));
    }

    return array;
}

/**
 * Accepts an `Observable` collection and returns a mapped collection where each instance will complete upon the its
 * next event.
 *
 * `Observable` completion is neccessary for the successful application of rxjs functions such as `forkJoin`,
 * and for the `Observable` `toPromise` method.
 * @param observable The input collection.
 * @param mapFn The function which is used to map the output of each source Observable.
 * @param mapErrorFn An optional function which is used to map any error originating from a source Observable.
 * If `mapErrorFn` is not provided, then errors will be unhandled.
 */
export function ensureAllCompleteWithMap<T, TMap>(
    observable: Observable<T>[],
    mapFn: (src: T) => TMap,
    mapErrorFn?: (error: any) => TMap
): Observable<TMap>[] {
    if (observable == undefined || observable.length === 0) return [];
    const array = [];
    for (const src of observable) {
        if (src == undefined) continue;
        let o = src.pipe(take(1), map(mapFn));
        if (mapErrorFn != undefined) {
            o = o.pipe(catchError(error => of(mapErrorFn(error))));
        }
        array.push(o);
    }

    return array;
}

/**
 * Safely unsubscribes from a `Subscription` by ignoring `null` or `undefined` references or subscriptions which are
 * already closed.
 *
 * Returns `true` if `unsubscribe()` was invoked on the subscription, otherwise returns `false`.
 *
 * @param subscription The subscription to conditionally unsubscribe from.
 */
export function safeUnsubscribe(
    subscription: SubscriptionLike | undefined
): boolean {
    if (subscription == undefined || subscription.closed) return false;
    subscription.unsubscribe();

    return true;
}

function resolveObserver<T>(
    next: PartialObserver<T> | ((value: T) => void),
    error?: (error: any) => void,
    complete?: () => void
): PartialObserver<T> {
    if (typeof next === "object") return next;

    return {
        complete,
        error,
        next
    };
}

function sequencer(actions: AwaitAction[]): Observable<void> {
    const result = new ReplaySubject<void>(1);
    let i = -1;

    const next = (): void => {
        i += 1;
        if (i < actions.length) {
            const a: Observable<any> | Observable<any>[] = actions[i]();
            awaitOne(Array.isArray(a) ? a : [a])
                .toPromise()
                .then(next)
                .catch(error => result.error(error));
        } else {
            result.next();
            result.complete();
        }
    };
    next();

    return result;
}
