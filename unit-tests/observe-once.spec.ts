import { Subject } from "rxjs";

import * as Async from "../src";

/* tslint:disable:no-magic-numbers*/

describe("async.observeOnce()", () => {
    it("Should only emit first source event.", () => {
        let onceCount = 0;
        let alwaysCount = 0;
        const subject = new Subject<void>();
        const obs = Async.observeOnce(subject);
        subject.subscribe(() => {
            alwaysCount += 1;
        });
        obs.subscribe(() => {
            onceCount += 1;
        });

        subject.next();
        subject.next();
        expect(onceCount)
            .withContext("Emitted more than once")
            .toBe(1);
        expect(alwaysCount)
            .withContext("Source observable did not continue emitting")
            .toBe(2);
    });

    it("Proxy subscription should close and complete after source emits, source subscription should not close or complete", () => {
        const trigger = new Subject<void>();
        const obs = Async.observeOnce(trigger);
        let triggerCompleted = false;
        let proxyCompleted = false;
        const proxySub = obs.subscribe({
            complete: () => {
                proxyCompleted = true;
            }
        });

        trigger.subscribe({
            complete: () => {
                triggerCompleted = true;
            }
        });

        expect(proxySub.closed)
            .withContext("Subscription closed prematurely")
            .toBe(false);
        trigger.next();
        expect(proxySub.closed)
            .withContext("Subscription did not close")
            .toBe(true);
        expect(trigger.closed)
            .withContext("Trigger closed")
            .toBe(false);
        expect(proxyCompleted)
            .withContext("Subscription was not completed.")
            .toBe(true);
        expect(triggerCompleted)
            .withContext("Trigger was completed.")
            .toBe(false);
    });

    it("Proxy observable should be multicast", () => {
        let i = 0;
        let proxiedEventCount = 0;
        const subject = new Subject<number>();
        subject.subscribe(value => {
            expect(value)
                .withContext(
                    "Source subcription value does not match active index"
                )
                .toBe(i);
        });
        const obs = Async.observeOnce(subject);
        while (i < 3) {
            i += 1;
            obs.subscribe(value => {
                proxiedEventCount += 1;
                expect(value)
                    .withContext(
                        "Proxy subcription value not first event value"
                    )
                    .toBe(1);
            });
            subject.next(i);
        }
        expect(proxiedEventCount)
            .withContext("Did not fire expected number of proxy events")
            .toBe(3);
    });

    it("Should convert to a promise when synchronous", async () => {
        let completed = false;
        const subject = new Subject<void>();
        const p = Async.observeOnce(subject)
            .toPromise()
            .then(() => {
                completed = true;
            });
        expect(completed)
            .withContext("Subscription completed prematurely")
            .toBe(false);
        subject.next();
        await expectAsync(p).toBeResolved();
        expect(completed)
            .withContext("Failed to complete")
            .toBe(true);
    }, 100);

    it("Should convert to a promise when asynchronous", async () => {
        let completed = false;
        const subject = new Subject<void>();
        const p = Async.observeOnce(subject)
            .toPromise()
            .then(() => {
                completed = true;
            });
        setTimeout(() => {
            subject.next();
        }, 20);
        expect(completed)
            .withContext("Subscription completed prematurely")
            .toBe(false);
        await expectAsync(p).toBeResolved();
        expect(completed)
            .withContext("Failed to complete")
            .toBe(true);
    }, 100);
});
