import { Observable, Subject, Subscription } from "rxjs";

import * as Async from "../src";

/* tslint:disable:no-magic-numbers*/

// #region Tester

class Tester {
    get promise(): Promise<void> {
        return this._promise;
    }
    private _completeCount = 0;
    private _error: any;
    private _errorInvoked = false;
    private _nextCount = 0;
    private _proceed: (() => void) | undefined;
    private _promise: Promise<void>;
    private _subscription: Subscription;

    private constructor() {}

    cancel(): void {
        if (!this._subscription.closed) {
            this._subscription.unsubscribe();
        }
        this.proceed();
    }

    /**
     * Expects that:
     *
     *   -- next() has NOT been invoked
     *
     *   -- complete() has NOT been invoked
     *
     *   -- the subscription is closed (unsubscribed from)
     *
     *   -- there has been NO error
     */
    expectCancelled(): void {
        this.expectNoNext();
        this.expectNotComplete();
        this.expectUnsubscribed();
        this.expectNoError();
    }

    expectComplete(context?: string): void {
        expect(this._completeCount)
            .withContext("Complete was invoked more than once.")
            .toBeLessThan(2);

        expect(this._completeCount)
            .withContext(
                context != undefined
                    ? context
                    : "The observable did not complete."
            )
            .toBe(1);
    }

    /**
     * Expects that:
     *
     *   -- complete() has NOT been invoked
     *
     *   -- the subscription is closed (unsubscribed from)
     */
    expectError(context?: string): void {
        expect(this._errorInvoked)
            .withContext(
                context != undefined
                    ? context
                    : "The error() handler was not invoked"
            )
            .toBe(true);
        this.expectNotComplete();
        this.expectUnsubscribed();
    }

    /**
     * Expects that:
     *
     *   -- An error of the specified value exists
     *
     *   -- complete() has NOT been invoked
     *
     *   -- the subscription is closed (unsubscribed from)
     */
    expectErrorValue(value: any, context?: string): void {
        this.expectError();
        expect(this._error)
            .withContext(
                context != undefined ? context : "Unexpected error value"
            )
            .toBe(value);
    }

    /**
     * Expects that:
     *
     *   -- next() has been invoked once
     *
     *   -- complete() has been invoked
     *
     *   -- the subscription was automatically closed (unsubscribed from)
     *
     *   -- there has been NO error
     */
    expectHandled(): void {
        this.expectNext();
        this.expectComplete();
        this.expectUnsubscribed();
        this.expectNoError();
    }

    expectNext(context?: string): void {
        expect(this._nextCount)
            .withContext(
                context != undefined
                    ? context
                    : "The next() handler was not invoked"
            )
            .toBeGreaterThan(0);

        expect(this._nextCount)
            .withContext("The next() handler was invoked more than once")
            .toBe(1);
    }

    expectNoError(context?: string): void {
        expect(this._errorInvoked)
            .withContext(
                context != undefined
                    ? context
                    : "The error() handler was invoked unexpectedly"
            )
            .toBe(false);
    }

    expectNoNext(context?: string): void {
        expect(this._nextCount)
            .withContext(
                context != undefined
                    ? context
                    : "The next() handler was not invoked"
            )
            .toBe(0);
    }

    expectNotComplete(context?: string): void {
        expect(this._completeCount)
            .withContext("Complete was invoked more than once.")
            .toBeLessThan(2);

        expect(this._completeCount)
            .withContext(
                context != undefined
                    ? context
                    : "The observable completed when it should not have."
            )
            .toBe(0);
    }

    expectNotUnsubcribed(context?: string): void {
        expect(this._subscription.closed)
            .withContext(
                context != undefined ? context : "Prematurely unsubscribed"
            )
            .toBe(false);
    }

    expectUnsubscribed(context?: string): void {
        expect(this._subscription.closed)
            .withContext(
                context != undefined ? context : "Failed to unsubscribe"
            )
            .toBe(true);
    }

    /**
     * Expects that:
     *
     *   -- next() has NOT been invoked
     *
     *   -- complete() has NOT been invoked
     *
     *   -- the subscription is NOT closed
     *
     *   -- there has been NO error
     */
    expectWaiting(): void {
        this.expectNoNext();
        this.expectNotComplete();
        this.expectNotUnsubcribed();
        this.expectNoError();
    }

    /**
     * Unsubscribes from the underlying subscription.
     */
    unsubscribe(): void {
        this._subscription.unsubscribe();
    }

    private proceed(): void {
        if (this._proceed != undefined) {
            this._proceed();
            this._proceed = undefined;
        }
    }

    static withLamdas(trigger: Observable<any>): Tester {
        const r = new Tester();
        const p = Async.openPromise<void>();
        r._promise = p.promise;
        r._proceed = p.resolve;
        r._subscription = Async.subscribeOnce(
            trigger,
            () => {
                r._nextCount += 1;
                if (r._nextCount === 1) {
                    r.proceed();
                }
            },
            e => {
                r._errorInvoked = true;
                r._error = e;
                r.proceed();
            },
            () => {
                r._completeCount += 1;
            }
        );

        return r;
    }

    static withObserver(trigger: Observable<any>): Tester {
        const r = new Tester();
        const p = Async.openPromise<void>();
        r._promise = p.promise;
        r._proceed = p.resolve;
        r._subscription = Async.subscribeOnce(trigger, {
            complete: () => {
                r._completeCount += 1;
            },
            error: e => {
                r._errorInvoked = true;
                r._error = e;
                r.proceed();
            },
            next: () => {
                r._nextCount += 1;
                if (r._nextCount === 1) {
                    r.proceed();
                }
            }
        });

        return r;
    }
}

// #endregion

describe("async.subscribeOnce()", () => {
    it("Should unsubscribe when handled synchronously", () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);
        trigger.next();
        tester.expectHandled();
    });

    it("Should unsubscribe when handled asynchronously", async () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);

        setTimeout(() => {
            trigger.next();
        }, 100);

        tester.expectWaiting();
        await expectAsync(tester.promise).toBeResolved();
        tester.expectHandled();
    }, 200);

    it("Should not emit when cancelled before asynchronous source event emits", async () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);

        setTimeout(() => {
            trigger.next();
            tester.cancel();
        }, 100);
        tester.unsubscribe();
        tester.expectUnsubscribed();
        await expectAsync(tester.promise).toBeResolved();
        tester.expectCancelled();
    }, 200);

    it("Should only emit the first source event when handled synchronously", () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);

        trigger.next();
        tester.expectNext("Did not complete synchronously");
        trigger.next();
        tester.expectNext();
    });

    it("Should only emit the first source event when handled asynchronously", async () => {
        const eventNumber = 3;
        let count = 0;
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);
        const p = Async.openPromise();

        const asyncNext = (step: number) => {
            if (step === 0) {
                p.resolve();
            } else {
                setTimeout(() => {
                    count += 1;
                    trigger.next();
                    asyncNext(step - 1);
                }, 10);
            }
        };

        asyncNext(eventNumber);
        await expectAsync(p.promise).toBeResolved();
        expect(count)
            .withContext("Did not emit the expected number of events")
            .toBe(eventNumber);
        tester.expectHandled();
    }, 200);

    it("Proxy should complete when source completes without emitting", () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);

        trigger.complete();
        tester.expectComplete();
    });

    it("Proxy should complete automatically after source emits once despite source not completing", () => {
        let triggerCompleted = false;
        const trigger = new Subject<void>();
        trigger.subscribe({
            complete: () => {
                triggerCompleted = true;
            }
        });
        const tester = Tester.withLamdas(trigger);

        trigger.next();
        tester.expectComplete();
        expect(triggerCompleted)
            .withContext("Source should not complete")
            .toBe(false);
    });

    it("Proxy should NOT complete when source completes with no emit after proxy cancelled", async () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);

        tester.cancel();
        trigger.complete();
        tester.expectCancelled();
    });

    it("Source error should be proxied", async () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);

        trigger.error("Failed");
        tester.expectErrorValue("Failed");
    });

    it("Source error should NOT be proxied if proxy subscription cancelled", async () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);
        tester.cancel();
        trigger.error("Failed");
        tester.expectCancelled();
    });

    it("Source error should NOT be proxied if source event has already been proxied", async () => {
        const trigger = new Subject<void>();
        const tester = Tester.withLamdas(trigger);
        trigger.next();
        trigger.error("Failed");
        tester.expectHandled();
    });
});
