import { forkJoin, Subject } from "rxjs";

import * as Async from "../src";

/* tslint:disable:no-magic-numbers*/

describe("async.ensureAllComplete()", () => {
    it("Should complete synchronously where all sources are synchronous", () => {
        let output: number[] | undefined;
        const src1 = new Subject<number>();
        const src2 = new Subject<number>();
        const src3 = new Subject<number>();
        const sub = forkJoin(
            Async.ensureAllComplete([
                src1.asObservable(),
                src2.asObservable(),
                src3.asObservable()
            ])
        ).subscribe(r => {
            output = r;
        });
        src1.next(1);
        src1.next(2);
        src2.next(2);
        src2.next(3);
        src3.next(3);
        src3.next(4);
        expect(output)
            .withContext("Did not complete synchronously")
            .toBeDefined();
        expect(output)
            .withContext("Unexpected output")
            .toEqual([1, 2, 3]);
        src3.next(5);
        expect(output)
            .withContext("New output after initial complete")
            .toEqual([1, 2, 3]);
        sub.unsubscribe();
    });

    it("Should complete asynchronously where a single source is asynchronous", async () => {
        const p = Async.openPromise<number[]>();
        let completed = false;
        const src1 = new Subject<number>();
        const src2 = new Subject<number>();
        const sub = forkJoin(
            Async.ensureAllComplete([
                src1.asObservable(),
                src2.asObservable(),
                Async.awaitDelay(50, 3)
            ])
        ).subscribe(r => {
            completed = true;
            p.resolve(r);
        });
        src1.next(1);
        src1.next(2);
        src2.next(2);
        src2.next(3);
        expect(completed)
            .withContext("Completed synchronously")
            .toBe(false);
        await expectAsync(p.promise)
            .withContext("Unexpected output")
            .toBeResolvedTo([1, 2, 3]);
        expect(completed)
            .withContext("Did not complete asynchronously")
            .toBe(true);
        sub.unsubscribe();
    }, 200);

    it("Should complete asynchronously where all sources are asynchronous", async () => {
        const p = Async.openPromise<number[]>();
        let completed = false;
        const sub = forkJoin(
            Async.ensureAllComplete([
                Async.awaitDelay(60, 1),
                Async.awaitDelay(40, 2),
                Async.awaitDelay(20, 3)
            ])
        ).subscribe(r => {
            completed = true;
            p.resolve(r);
        });
        expect(completed)
            .withContext("Should not complete synchronously")
            .toBe(false);
        await expectAsync(p.promise)
            .withContext("Unexpected output")
            .toBeResolvedTo([1, 2, 3]);
        expect(completed)
            .withContext("Did not complete asynchronously")
            .toBe(true);
        sub.unsubscribe();
    }, 200);

    errorTest(true);

    errorTest(false);
});

function errorTest(synchronous: boolean): void {
    it(`Should fail with an error where a source errors ${
        synchronous ? "synchronously" : "asynchronously"
    }`, async () => {
        const p = Async.openPromise<number[]>();
        let completed = false;
        const src1 = new Subject<number>();
        const src2 = new Subject<number>();
        const src3 = new Subject<number>();
        const sub = forkJoin(
            Async.ensureAllComplete([
                src1.asObservable(),
                src2.asObservable(),
                src3.asObservable()
            ])
        ).subscribe(
            () => {
                completed = true;
            },
            e => {
                completed = true;
                p.reject(e);
            }
        );
        src1.next(1);
        src2.next(2);
        if (synchronous) {
            src3.error("Failed");
            expect(completed)
                .withContext("Did not completed synchronously")
                .toBe(true);
        } else {
            setTimeout(() => {
                src3.error("Failed");
            }, 50);
            expect(completed)
                .withContext("Did not completed asynchronously")
                .toBe(false);
        }
        await expectAsync(p.promise)
            .withContext("Should fail with error")
            .toBeRejectedWith("Failed");
        sub.unsubscribe();
    }, 200);
}
