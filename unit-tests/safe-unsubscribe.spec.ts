import { Subject } from "rxjs";

import * as Async from "../src";

describe("async.safeUnsubscribe()", () => {
    it("Should close an open subscription", () => {
        const subject = new Subject();
        const subscription = subject.subscribe(() => {});
        expect(subscription.closed).toBe(false);
        expect(Async.safeUnsubscribe(subscription)).toBe(true);
        expect(subscription.closed).toBe(true);
    });

    it("Should NOT close an already closed subscription", () => {
        const subject = new Subject();
        // tslint:disable-next-line: no-empty
        const subscription = subject.subscribe(() => {});
        subscription.unsubscribe();
        expect(subscription.closed).toBe(true);
        expect(Async.safeUnsubscribe(subscription)).toBe(false);
    });

    it("Should safely handle null and undefined subscription references", () => {
        // tslint:disable-next-line: no-unsafe-any
        expect(Async.safeUnsubscribe(getUndefined())).toBe(false);
        // tslint:disable-next-line: no-null-keyword
        // tslint:disable-next-line: no-unsafe-any
        expect(Async.safeUnsubscribe(getNull())).toBe(false);
    });
});

function getNull(): any {
    // tslint:disable-next-line: no-null-keyword
    return null;
}

function getUndefined(): any {
    return undefined;
}
