import { Subject } from "rxjs";

import * as Async from "../src";

/* tslint:disable:no-magic-numbers*/

describe("async.ensureComplete()", () => {
    it("Should wrap an open source observable in one that completes on its first emit and ignores subsequent emits from the source", () => {
        let sourceCount = 0;
        let wrappedCount = 0;
        const subject = new Subject();
        const srcObs = subject.asObservable();
        const srcSub = srcObs.subscribe({
            complete: () => {
                sourceCount += 1;
            }
        });
        expect(srcSub.closed)
            .withContext("The source subscription should be open")
            .toBe(false);
        subject.next();
        expect(sourceCount)
            .withContext("The original observable should not complete")
            .toBe(0);
        const wrappedObs = Async.ensureComplete(srcObs);
        const wrappedSub = wrappedObs.subscribe({
            complete: () => {
                wrappedCount += 1;
            }
        });
        expect(wrappedSub.closed)
            .withContext("The wrapped subscription should be open")
            .toBe(false);
        expect(wrappedCount)
            .withContext(
                "The previous next call should not affect the wrapped observable"
            )
            .toBe(0);
        expect(sourceCount)
            .withContext(
                "The original observable should not be affected by the ensureComlete operation"
            )
            .toBe(0);
        subject.next();
        expect(sourceCount)
            .withContext("The original observable should still not complete")
            .toBe(0);
        expect(wrappedCount)
            .withContext("The wrapped observable should have completed")
            .toBe(1);
        expect(wrappedSub.closed)
            .withContext("The wrapped subscription should be closed")
            .toBe(true);
        subject.next();
        expect(wrappedCount)
            .withContext("The wrapped observable should not emit again")
            .toBe(1);
        expect(srcSub.closed)
            .withContext("The source observable should be open")
            .toBe(false);
    });

    // tslint:disable-next-line: max-line-length
    it("Should wrap an open source observable in one that resolves using toPromise() and ignores subsequent emits from the source", async () => {
        const subject = new Subject<number>();
        subject.next(1);
        const obs = Async.ensureComplete(subject.asObservable());
        subject.next(2);
        const p = obs.toPromise();
        subject.next(3);
        subject.next(4);
        await expectAsync(p)
            .withContext("Unexpected output.")
            .toBeResolvedTo(3);
    }, 200);
});
