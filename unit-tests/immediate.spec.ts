import * as Async from "../src";

/* tslint:disable:no-magic-numbers*/

describe("async.immediate()", () => {
    it("Should be multicast", () => {
        let proxiedEventCount = 0;
        const obs = Async.immediate(true);
        for (let i = 0; i < 3; i += 1) {
            obs.subscribe(() => {
                proxiedEventCount += 1;
            });
        }
        expect(proxiedEventCount)
            .withContext("Did not fire expected number of proxied events")
            .toBe(3);
    });
});
