import { forkJoin, Observable, of, ReplaySubject, Subject } from "rxjs";
import { tap } from "rxjs/operators";

import * as Async from "../src";

/* tslint:disable:no-magic-numbers*/

const shortTimeout = 200;
const longTimeout = 500;

describe("async.awaitSequence()", () => {
    it(
        "Sequence of single synchronous actions should execute sequentially",
        async () => singleActions(false),
        shortTimeout
    );
    it(
        "Sequence of single asynchronous actions should execute sequentially",
        async () => singleActions(true),
        shortTimeout
    );

    it(
        "Sequence of multiple synchronous actions should execute sequentially",
        async () => multipleActions(false),
        shortTimeout
    );
    it(
        "Sequence of multiple asynchronous actions should execute sequentially",
        async () => multipleActions(true),
        shortTimeout
    );

    it(
        "Sequence should execute in ascending order despite its await actions completing in descending order",
        async () => {
            const opCount = 10;
            const triggers: Subject<number>[] = new Array<Subject<number>>(
                opCount
            );
            const actions: Async.AwaitAction[] = [];
            const triggerSequence: number[] = [];
            const executionSequence: number[] = [];

            const factory = (index: number): (() => Observable<number>) => {
                triggers[index] = new Subject();
                const localAction = new ReplaySubject<number>(1);
                Async.subscribeOnce(triggers[index], (value: number) => {
                    triggerSequence.push(value);
                    localAction.next(value);
                });

                return () => {
                    executionSequence.push(index);

                    return localAction;
                };
            };

            for (let i = 0; i < opCount; i += 1) {
                actions.push(factory(i));
            }

            const executeActionRecursive = (idx: number): void => {
                if (idx < 0) return;
                setTimeout(() => {
                    Async.subscribeOnce(triggers[idx], () => {
                        executeActionRecursive(idx - 1);
                    });
                    triggers[idx].next(idx);
                }, 5);
            };

            executeActionRecursive(opCount - 1);
            await Async.awaitSequence(...actions).toPromise();

            // The sequence in which the background trigger events (which cause each AwaitAction to emit) are expected to complete.
            // Upon completion, each should then trigger the preceding event.
            // The process should initiate with the last event in the sequence, and conclude with the first.
            expect(triggerSequence)
                .withContext("Unexpected event sequence")
                // tslint:disable-next-line: no-magic-numbers
                .toEqual([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);

            // The order in which each AwaitAction (which Async.awaitSequence awaits upon in sequence) is expected to complete.
            // Even though the trigger events for each AwaitAction finish in reverse order (starting with the last and ending with
            // the first as described in the triggerSequence above), the await actions should execute in ascending order, starting
            // with the first in the sequence, and with each subsequent action only executing after its immediate predecessor has
            // execute successfully.
            expect(executionSequence)
                .withContext("Unexpected execution sequence")
                // tslint:disable-next-line: no-magic-numbers
                .toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        },
        longTimeout
    );

    it(
        "Sequence of single synchronous actions should fail with single error",
        async () => failSingleAction(false, false),
        shortTimeout
    );

    it(
        "Sequence of single asynchronous actions should fail with single error",
        async () => failSingleAction(false, true),
        shortTimeout
    );

    it(
        "Sequence of multiple synchronous actions should fail with single error",
        async () => failSingleAction(true, false),
        shortTimeout
    );

    it(
        "Sequence of multiple asynchronous actions should fail with single error",
        async () => failSingleAction(true, true),
        shortTimeout
    );
});

async function singleActions(doAsync: boolean) {
    const opCount = 10;
    const actions: Async.AwaitAction[] = [];
    const executionSequence: number[] = [];
    const output: number[] = [];

    const factory = (index: number): (() => Observable<number>) => () => {
        executionSequence.push(index);
        const obs = doAsync ? Async.awaitDelay(5, index) : of(index);

        return obs.pipe(
            tap(idx => {
                output.push(idx);
            })
        );
    };

    for (let i = 0; i < opCount; i += 1) {
        actions.push(factory(i));
    }

    await Async.awaitSequence(...actions).toPromise();

    expect(executionSequence)
        .withContext("Unexpected execution sequence")
        // tslint:disable-next-line: no-magic-numbers
        .toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

    expect(output)
        .withContext("Unexpected final output")
        // tslint:disable-next-line: no-magic-numbers
        .toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
}

async function multipleActions(doAsync: boolean) {
    const opCount = 10;
    const actions: Async.AwaitAction[] = [];
    const executionSequence: number[] = [];
    const output: number[] = [];

    const factory = (index: number): (() => Observable<number>[]) => () => {
        executionSequence.push(index);
        const mark = index * 3;
        const obs = doAsync
            ? [of(mark + 1), of(mark + 2), of(mark + 3)]
            : [
                  Async.awaitDelay(5, mark + 1),
                  Async.awaitDelay(5, mark + 2),
                  Async.awaitDelay(5, mark + 3)
              ];

        forkJoin<number>(Async.ensureAllComplete(obs)).subscribe(array => {
            output.push(sumAll(array));
        });

        return obs;
    };

    for (let i = 0; i < opCount; i += 1) {
        actions.push(factory(i));
    }

    await Async.awaitSequence(...actions).toPromise();

    expect(executionSequence)
        .withContext("Unexpected execution sequence")
        // tslint:disable-next-line: no-magic-numbers
        .toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);

    expect(output)
        .withContext("Unexpected final output")
        // tslint:disable-next-line: no-magic-numbers
        .toEqual([6, 15, 24, 33, 42, 51, 60, 69, 78, 87]);
}

async function failSingleAction(multiple: boolean, doAsync: boolean) {
    const delay = 50;
    function f(i?: number): () => Observable<number> {
        let r: () => Observable<number>;
        if (doAsync) {
            r = () =>
                i != undefined
                    ? Async.awaitDelay(delay, i)
                    : createSubjectAndError("Failed", true);
        } else {
            r = () =>
                i != undefined
                    ? Async.immediate(i)
                    : createSubjectAndError("Failed");
        }

        return r;
    }

    function fm(i?: number): () => Observable<number>[] {
        let r: () => Observable<number>[];
        if (doAsync) {
            r = () =>
                i != undefined
                    ? [Async.awaitDelay(delay, i), Async.awaitDelay(delay, i)]
                    : [
                          Async.awaitDelay(delay, i),
                          createSubjectAndError("Failed", true)
                      ];
        } else {
            r = () =>
                i != undefined
                    ? [Async.immediate(i), Async.immediate(i)]
                    : [Async.immediate(i), createSubjectAndError("Failed")];
        }

        return r;
    }

    let caught: any = "No Error Raised";
    const actions: Async.AwaitAction[] = multiple
        ? [fm(1), fm(), fm(2)]
        : [f(1), f(), f(2)];
    await Async.awaitSequence(...actions)
        .toPromise()
        .catch(e => {
            caught = e;
        });
    expect(caught).toBe("Failed");
}

function sumAll(array: number[]): number {
    let r = 0;
    for (const v of array) {
        r += v != undefined ? v : 0;
    }

    return r;
}

function createSubjectAndError(
    error: any,
    doAsync: boolean = false
): Observable<any> {
    const delay = 50;
    const r = new Subject();
    if (doAsync) {
        setTimeout(() => {
            r.error(error);
        }, delay);
    } else {
        r.error(error);
    }

    return r;
}
